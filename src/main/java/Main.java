import java.io.*;
import java.text.ParseException;
import java.util.Scanner;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Main {

    public static void main(String[] args) throws ParseException, IOException, org.json.simple.parser.ParseException, ParserConfigurationException, TransformerException, SAXException {

        //Note: Функции разделены по пунктам задания, для использования раскомментируйте нужные функции

        // №3
        //FileSystemInfo();
        // №2
        //WriteFile();
        //ReadFile();
        // №3
        //JsonWriter();
        //JsonReader();
        // №4
        //WriteXML();
        // №5
        //AddZip();
        //ExtractZip();
    }

    public static void FileSystemInfo()
    {
        System.out.println("File system roots returned byFileSystemView.getFileSystemView():");
        FileSystemView fsv = FileSystemView.getFileSystemView();
        File[] roots = fsv.getRoots();
        for (int i = 0; i < roots.length; i++) {
            System.out.println("Root: " + roots[i]);
        }

        System.out.println("Home directory: " + fsv.getHomeDirectory());

        System.out.println("File system roots returned by File.listRoots():");
        File[] f = File.listRoots();
        for (int i = 0; i < f.length; i++) {
            System.out.println("Drive: " + f[i]);
            System.out.println("Display name: " + fsv.getSystemDisplayName(f[i]));
            System.out.println("Is drive: " + fsv.isDrive(f[i]));
            System.out.println("Is floppy: " + fsv.isFloppyDrive(f[i]));
            System.out.println("Readable: " + f[i].canRead());
            System.out.println("Writable: " + f[i].canWrite());
            System.out.println("Total space: " + f[i].getTotalSpace());
            System.out.println("Usable space: " + f[i].getUsableSpace());
        }
    }

    public static void WriteFile(){
        try(FileWriter writer = new FileWriter("fileFor2ndTask.txt", false))
        {
            // запись всей строки
            Scanner console = new Scanner(System.in);
            System.out.println("Введите строку для записи в файл");
            String text = console.nextLine();
            writer.write(text);

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
    public static void ReadFile()
    {
        try(FileReader reader = new FileReader("fileFor2ndTask.txt"))
        {
            // читаем посимвольно
            int c;
            System.out.println("Содержание файла fileFor2ndTask.txt: ");
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
            System.out.println();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public static void JsonWriter(){
        JSONObject obj = new JSONObject();

        obj.put("name", "Daniil Purtov");
        obj.put("age", new Integer(21));

        JSONArray cities = new JSONArray();
        cities.add("Moscow");
        cities.add("Kursk");
        cities.add("Rostov");

        obj.put("cities", cities);

        try {
            FileWriter file = new FileWriter("fileFor3dTask.json");
            file.write(obj.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void JsonReader() throws ParseException, FileNotFoundException, IOException, org.json.simple.parser.ParseException {
        try {

            JSONParser parser = new JSONParser();
            Reader reader = new FileReader("fileFor3dTask.json");

            Object jsonObj = parser.parse(reader);

            JSONObject jsonObject = (JSONObject) jsonObj;
            System.out.println("Содержание fileFor3dTask.json: ");
            String name = (String) jsonObject.get("name");
            System.out.println("Name = " + name);

            long age = (Long) jsonObject.get("age");
            System.out.println("Age = " + age);

            JSONArray cities = (JSONArray) jsonObject.get("cities");

            @SuppressWarnings("unchecked")
            Iterator it = cities.iterator();
            while (it.hasNext()) {
                System.out.println("City = " + it.next());
            }
            reader.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
    public static void WriteXML() throws ParserConfigurationException, IOException, SAXException, TransformerException {
        File fXmlFile = new File("fileFor4thTask.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        Element nList = doc.getDocumentElement();

        System.out.println("-----------------------");

        Element item = doc.createElement("item");

        Attr name = doc.createAttribute("Name");
        name.setValue("Vasya");
        Attr city = doc.createAttribute("City");
        city.setValue("Kaluga");
        item.setAttributeNode(name);
        item.setAttributeNode(city);

        nList.appendChild(item);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new File("fileFor4thTask.xml"));
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        System.out.println("DONE");
    }

    public static void AddZip()
    {
        JFileChooser fileopen = new JFileChooser("./");
        int ret = fileopen.showDialog(null, "Открыть файл");
        String filename = fileopen.getSelectedFile().getName();
        try(ZipOutputStream zout = new ZipOutputStream(new FileOutputStream("output.zip"));
            FileInputStream fis= new FileInputStream(filename);)
        {
            ZipEntry entry1=new ZipEntry(filename);
            zout.putNextEntry(entry1);
            // считываем содержимое файла в массив byte
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            // добавляем содержимое к архиву
            zout.write(buffer);
            // закрываем текущую запись для новой записи
            zout.closeEntry();
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void ExtractZip()
    {
        try(ZipInputStream zin = new ZipInputStream(new FileInputStream("output.zip")))
        {
            ZipEntry entry;
            String name;
            long size;
            while((entry=zin.getNextEntry())!=null){

                name = entry.getName(); // получим название файла
                size=entry.getSize();  // получим его размер в байтах
                System.out.printf("File name: %s \t File size: %d \n", name, size);

                // распаковка
                FileOutputStream fout = new FileOutputStream(name);
                for (int c = zin.read(); c != -1; c = zin.read()) {
                    fout.write(c);
                }
                fout.flush();
                zin.closeEntry();
                fout.close();
            }
        }
        catch(Exception ex){

            System.out.println(ex.getMessage());
        }
    }

}
